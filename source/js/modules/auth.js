class Authentication {
  static authenticate(tkn) {
    $.ajax({
      url: "actions/curl.php",
      type: "POST",
      data: { req: { tkn: tkn } },
      dataType: "json",
      success: (echo) => {
        this.handleResponse(echo, tkn);
      },
      error: (e) => {
        sessionStorage.setItem("auth", false);
        Swal.fire({
          icon: "error",
          title: "Opa...",
          text: `Verifique o token digitado`,
        });
      },
    });
  }

  static verifyAuth(auth) {
    if (auth == false || auth == "false") {
      this.logout();
      return false;
    } else {
      return true;
    }
  }

  static logout() {
    sessionStorage.clear();
    window.location.replace("index.html");
  }

  static sessionSet(value, expirationInMin = 10) {
    const expirationDate = new Date(
      new Date().getTime() + 60000 * expirationInMin
    );
    const newValue = {
      value: value,
      expirationDate: expirationDate.toISOString(),
    };
    sessionStorage.setItem("expireSession", JSON.stringify(newValue));
  }

  static sessionGet() {
    const stringValue = window.sessionStorage.getItem("expireSession")
      if (stringValue !== null) {
        const value = JSON.parse(stringValue)
        const expirationDate = new Date(value.expirationDate)
          if (expirationDate > new Date()) {
            return value.value
          } else {
            window.sessionStorage.removeItem("expireSession")
          }
      }
      return null
  }
}

export { Authentication };
