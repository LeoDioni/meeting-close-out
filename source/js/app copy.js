//////Login basico////

Swal.fire({
  title: 'Login',
  html: ` <input type="password" id="senha" class="swal2-input" placeholder="Senha padrão">`,
  confirmButtonText: 'Entrar',
  allowOutsideClick: false,
  allowEscapeKey: false,
  allowEnterKey: false,
  focusConfirm: false,
  preConfirm: () => {

    const password = Swal.getPopup().querySelector('#senha').value
    if (password!="orders") {
      Swal.showValidationMessage(`Senha padrão incorreta!`)
    }
  }
}).then((result) => {
  Swal.fire(`
    Autenticado(a)!
  `)
})


//Radio Satisfacao
document.getElementById("nao_satis").hidden=false;
document.getElementById("sim_satis").hidden=true;

document.getElementById("RadioSatisfacao1").addEventListener("click", function(){
  document.getElementById("nao_satis").hidden=true;
  document.getElementById("sim_satis").hidden=false;
});
  
document.getElementById("RadioSatisfacao2").addEventListener("click", function(){
  document.getElementById("nao_satis").hidden=false;
  document.getElementById("sim_satis").hidden=true;
});

////////////////dro/////////////////

document.getElementById("mes1").addEventListener("change", function(){
  mediadro();
});

document.getElementById("mes2").addEventListener("change", function(){
  mediadro();
});

document.getElementById("mes3").addEventListener("change", function(){
  mediadro();
});

document.getElementById("mes4").addEventListener("change", function(){
  mediadro();
 });

 document.getElementById("mes5").addEventListener("change", function(){
  mediadro();
 });

 document.getElementById("mes6").addEventListener("change", function(){
  mediadro();
 });

 document.getElementById("mes7").addEventListener("change", function(){
  mediadro();
 });

 document.getElementById("mes8").addEventListener("change", function(){
  mediadro();
 });

 document.getElementById("mes9").addEventListener("change", function(){
  mediadro();
 });

 document.getElementById("mes10").addEventListener("change", function(){
  mediadro();
 });

 document.getElementById("mes11").addEventListener("change", function(){
  mediadro();
 });

 document.getElementById("mes12").addEventListener("change", function(){
  mediadro();
 });

//////variação de termino
document.getElementById("Termino").addEventListener("change", function(){

    var dateend = moment(document.getElementById("Termino").value.substring(0,10), "DD/MM/YYYY");
    var datebegin = moment(document.getElementById("Inicio").value.substring(0,10), "DD/MM/YYYY");

    if ((dateend._d)!="Invalid Date"){

      var conta1 = Math.abs(dateend - datebegin);
      var conta2 = Math.ceil(conta1 / (1000 * 60 * 60 * 24)); 
      document.getElementById("duracao_encerramento").innerHTML = ((conta2)/30).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
      document.getElementById("duracao_variacao").value = ((!(isNaN(document.getElementById("duracao_encerramento").value))) || (!(isNaN(document.getElementById("duracao_handover").value))))  ?  (parseFloat(document.getElementById("duracao_encerramento").value)-parseFloat(document.getElementById("duracao_handover").value)).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2}) : "Data inválida";
    }
    else{document.getElementById("duracao_encerramento").innerHTML = "Data inválida"}

  });

  document.getElementById("duracao_handover").addEventListener("change", function(){
    debugger
    document.getElementById("duracao_variacao").value = ((!(isNaN(document.getElementById("duracao_encerramento").value))) || (!(isNaN(document.getElementById("duracao_handover").value))))  ?  (parseFloat(document.getElementById("duracao_encerramento").value)-parseFloat(document.getElementById("duracao_handover").value)).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2}) : "Data inválida";
    document.getElementById("duracao_variacao").innerHTML = document.getElementById("duracao_variacao").value
  });

//cor na bolinha/////
/////////////////// seção 8 ///////////////////////
document.getElementById("Composicao").addEventListener("click", function(){
  document.getElementById("sinal").style.backgroundColor = "green";
  document.getElementById("sinal2").style.backgroundColor = "green";
})

document.getElementById("PR").addEventListener("click", function(){
  document.getElementById("sinal").style.backgroundColor = "yellow";
  document.getElementById("sinal2").style.backgroundColor = "yellow";
})

document.getElementById("NMAP").addEventListener("click", function(){
  document.getElementById("sinal").style.backgroundColor = "red";
  document.getElementById("sinal2").style.backgroundColor = "red";
})

document.getElementById("NA").addEventListener("click", function(){
  document.getElementById("sinal").style.backgroundColor = "black";
  document.getElementById("sinal2").style.backgroundColor = "black";
})

////////////////////////////data/////////////////////////////////////qq
var Reunião ="";
var data = new Date();
var atual =  data.getDate() + '/' + (data.getMonth()+1) + '/' + data.getFullYear();
document.getElementById("atual").value=atual;
var data2 = new Date(data.getFullYear(), data.getMonth(), data.getDate());

////////////enviar formulario//////////////////////
$("#btn-init-survey").click(function () {  

  document.getElementById("mensagem").innerHTML ="Enviando formulário...";
  document.getElementById("carregando").hidden = false;

  var obj = {
    Proposta: $('#Proposta').val().toString(),
    Contrato: $('#Contrato').val().toString(),
    CC: $('#CC').val().toString(),
    CC_Nome: $('#CC_Nome').val().toString(),
    Diretor: $('#Diretor').val().toString(),
    Coordenador: $('#Coordenador').val().toString(),
    PMO: $('#PMO').val().toString(),
    Gerente: $('#Gerente').val().toString(),
    Elaboracao: $('#Elaboracao').val().toString(),
    Controller: $('#Controller').val().toString(),
    Cliente: $('#Grupo').val().toString(),
    CNPJ: $('#CNPJ').val().toString(),
    Inicio: $('#Inicio').val().toString(),
    Termino: $('#Termino').val().toString(),
    Tipo_Receita: $('#Tipo_Receita').val().toString(),
    Tipo_servico: $('#Tipo_servico').val().toString(),
    BE: $('#BE').val().toString(),
    Venda: $('#Venda').val().toString(),
    VTH: $('#Valor_total_handover').val().toString(),
    GRH: $('#Gross_revenue_handover').val().toString(),
    NRH: $('#Net_revenue_handover').val().toString(),
    GMH: $('#gm-net_handover').val().toString(),
    GrossH: $('#gross_margin_handover').val().toString(),
    MBH: $('#margem_bruta_handover').val().toString(),
    DuracaoH: $('#duracao_handover').val().toString(),
    CategoriaH: $('#categoria_handover').val().toString(),
    VTT: $('#Valor_total_target').val().toString(),
    GRT: $('#Gross_revenue_target').val().toString(),
    NRT: $('#Net_revenue_target').val().toString(),
    GMT: $('#gm-net_target').val().toString(),
    GrossT: $('#gross_margin_target').val().toString(),
    MBT: $('#margem_bruta_target').val().toString(),
    VTE: $('#Valor_total_encerramento').val().toString(),
    GRE: $('#Gross_revenue_encerramento').val().toString(),
    NRE: $('#Net_revenue_encerramento').val().toString(),
    GME: $('#gm-net_encerramento').val().toString(),
    GrossE: $('#gross_margin_encerramento').val().toString(),
    MBE: $('#margem_bruta_encerramento').val().toString(),
    DuracaoE: $('#duracao_encerramento').val().toString(),
    CategoriaE: document.getElementById("categoria_encerramento").innerHTML,
    VTV: $('#Valor_total_variacao').val().toString(),
    GRV: $('#Gross_revenue_variacao').val().toString(),
    NRV: $('#Net_revenue_variacao').val().toString(),
    GNV: $('#gm-net_variacao').val().toString(),
    GrossV: $('#gross_margin_variacao').val().toString(),
    MBV: $('#margem_bruta_variacao').val().toString(),
    DuracaoV: $('#duracao_variacao').val().toString(),
    Profit: $('#profit').val().toString(),
    MEPC1: $('#analise1').val().toString(),
    MEPC2: $('#analise2').val().toString(),
    MEPC3: $('#analise3').val().toString(),
    RadioServico1: document.getElementById("RadioServico1").checked,
    RadioServico2: document.getElementById("RadioServico2").checked,
    servico_fatura: $('#servico_fatura').val().toString(),
    RadioFatura1: document.getElementById("RadioFatura1").checked,
    RadioFatura2: document.getElementById("RadioFatura2").checked,
    servico_receber: $('#servico_receber').val().toString(),
    RadioRetencao1: document.getElementById("RadioRetencao1").checked,
    RadioRetencao2: document.getElementById("RadioRetencao2").checked,
    causa_retencao: $('#causa_retencao').val().toString(),
    RadioBack1: document.getElementById("RadioBack1").checked,
    RadioBack2: document.getElementById("RadioBack2").checked,
    backlog: $('#backlog').val().toString(),
    RadioCancel1: document.getElementById("RadioCancelamento1").checked,
    RadioCancel2: document.getElementById("RadioCancelamento2").checked,
    cancelamento: $('#cancelamento').val().toString(),
    dro_target: $('#drotarget').val().toString(),
    LM: $('#LM').val().toString(),
    LM_just: $('#LM_just').val().toString(),
    Venda_ho: $('#Venda_ho').val().toString(),
    venda_ho_just: $('#venda_ho_just').val().toString(),
    ho_target: $('#ho_target').val().toString(),
    ho_target_just: $('#ho_target_just').val().toString(),
    target_encerramento: $('#target_encerramento').val().toString(),
    target_encerramento_just: $('#target_encerramento_just').val().toString(),
    RadioEncerramento1: document.getElementById("RadioEncerramento1").checked,
    RadioEncerramento2: document.getElementById("RadioEncerramento2").checked,
    Acoes_encerramento: $('#Acoes_encerramento').val().toString(),
    responsavel_encerramento: $('#responsavel_encerramento').val().toString(),
    prazo_encerramento: $('#prazo_encerramento').val().toString(),
    RadioJuridico1: document.getElementById("RadioJuridico1").checked,
    RadioJuridico2: document.getElementById("RadioJuridico2").checked,
    Acoes_juridico: $('#Acoes_juridico').val().toString(),
    responsavel_juridico: $('#responsavel_juridico').val().toString(),
    prazo_juridico: $('#prazo_juridico').val().toString(),
    RadioInconsistencia1: document.getElementById("RadioInconsistencia1").checked,
    RadioInconsistencia2: document.getElementById("RadioInconsistencia2").checked,
    Acoes_ativo: $('#Acoes_ativo').val().toString(),
    responsavel_ativo: $('#responsavel_ativo').val().toString(),
    prazo_ativo: $('#prazo_ativo').val().toString(),
    RadioRecurso1: document.getElementById("RadioRecurso1").checked,
    RadioRecurso2: document.getElementById("RadioRecurso2").checked,
    Acoes_recurso: $('#Acoes_recurso').val().toString(),
    responsavel_recurso: $('#responsavel_recurso').val().toString(),
    prazo_recurso: $('#prazo_recurso').val().toString(),
    RadioRateio1: document.getElementById("RadioRateio1").checked,
    RadioRateio2: document.getElementById("RadioRateio2").checked,
    Acoes_rateio: $('#Acoes_rateio').val().toString(),
    responsavel_rateio: $('#responsavel_rateio').val().toString(),
    prazo_rateio: $('#prazo_rateio').val().toString(),
    RadioFixo1: document.getElementById("RadioFixo1").checked,
    RadioFixo2: document.getElementById("RadioFixo2").checked,
    Acoes_fixo: $('#Acoes_fixo').val().toString(),
    responsavel_fixo: $('#responsavel_fixo').val().toString(),
    prazo_fixo: $('#prazo_fixo').val().toString(),
    RadioConsorcio1: document.getElementById("RadioConsorcio1").checked,
    RadioConsorcio2: document.getElementById("RadioConsorcio2").checked,
    RadioBaixaOrgao1: document.getElementById("RadioBaixaOrgao1").checked,
    RadioBaixaOrgao2: document.getElementById("RadioBaixaOrgao2").checked,
    RadioLider1: document.getElementById("RadioLider1").checked,
    RadioLider2: document.getElementById("RadioLider2").checked,
    RadioDistrato1: document.getElementById("RadioDistrato1").checked,
    RadioDistrato2: document.getElementById("RadioDistrato2").checked,
    RadioQuitacao1: document.getElementById("RadioQuitacao1").checked,
    RadioQuitacao2: document.getElementById("RadioQuitacao2").checked,
    RadioBaixa1: document.getElementById("RadioBaixa1").checked,
    RadioBaixa2: document.getElementById("RadioBaixa2").checked,
    Acoes_consorcio: $('#Acoes_consorcio').val().toString(),
    responsavel_consorcio: $('#responsavel_consorcio').val().toString(),
    prazo_consorcio: $('#prazo_consorcio').val().toString(),
    aspectos: $('#aspectos').val().toString(),
    RadioSatisfacao1: document.getElementById("RadioSatisfacao1").checked,
    RadioSatisfacao2: document.getElementById("RadioSatisfacao2").checked,
    satisfacao_nome: $('#satisfacao_nome').val().toString(),
    satisfacao_contato: $('#satisfacao_contato').val().toString(),
    satisfacao_just: $('#satisfacao_just').val().toString(),
    registro: $('#registros').val().toString(),
    aditivos: $('#aditivos').val().toString(),
    RadioArt1: document.getElementById("RadioArt1").checked,
    RadioArt2: document.getElementById("RadioArt2").checked,
    Acoes_art: $('#Acoes_art').val().toString(),
    responsavel_art: $('#responsavel_art').val().toString(),
    prazo_art: $('#prazo_art').val().toString(),
    RadioAtestado1: document.getElementById("RadioAtestado1").checked,
    RadioAtestado2: document.getElementById("RadioAtestado2").checked,
    acoes_atestado: $('#acoes_atestado').val().toString(),
    responsavel_atestado: $('#responsavel_atestado').val().toString(),
    prazo_atestado: $('#prazo_atestado').val().toString(),
    RadioArquivado1: document.getElementById("RadioArquivado1").checked,
    RadioArquivado2: document.getElementById("RadioArquivado2").checked,
    Acoes_contrato: $('#Acoes_contrato').val().toString(),
    responsavel_contrato: $('#responsavel_contrato').val().toString(),
    prazo_contrato: $('#prazo_contrato').val().toString(),
    local: $('#local').val().toString(),
    local2: $('#local2').val().toString(),
    responsavel_local: $('#responsavel_local').val().toString(),
    local_prazo: $('#local_prazo').val().toString(),
    gng_ho: $('#gng_ho').val().toString(),
    project_review: $('#project_review').val().toString(),
    licoes_aprendidas: $('#licoes_aprendidas').val().toString(),
    RadioPrazo1: document.getElementById("RadioPrazo1").checked,
    RadioPrazo2: document.getElementById("RadioPrazo2").checked,
    prazo_realizado: $('#prazo_realizado').val().toString(),
    RadioCusto1: document.getElementById("RadioCusto1").checked,
    RadioCusto2: document.getElementById("RadioCusto2").checked,
    custo_efetivo: $('#custo_efetivo').val().toString(),
    RadioAlteracao1: document.getElementById("RadioAlteracao1").checked,
    RadioAlteracao2: document.getElementById("RadioAlteracao2").checked,
    alteracao_requisito: $('#alteracao_requisito').val().toString(),
    RadioDesvio1: document.getElementById("RadioDesvio1").checked,
    RadioDesvio2: document.getElementById("RadioDesvio2").checked,
    desvios_evitados: $('#desvios_evitados').val().toString(),
    RadioComunicacao1: document.getElementById("RadioComunicacao1").checked,
    RadioComunicacao2: document.getElementById("RadioComunicacao2").checked,
    comunicacao: $('#comunicacao').val().toString(),
    RadioAdm1: document.getElementById("RadioAdm1").checked,
    RadioAdm2: document.getElementById("RadioAdm2").checked,
    adm: $('#adm').val().toString(),
    RadioEntrega1: document.getElementById("RadioEntrega1").checked,
    RadioEntrega2: document.getElementById("RadioEntrega2").checked,
    entrega: $('#entrega').val().toString(),
    RadioCapacidade1: document.getElementById("RadioCapacidade1").checked,
    RadioCapacidade2: document.getElementById("RadioCapacidade2").checked,
    capacidade: $('#capacidade').val().toString(),
    RadioParceiro1: document.getElementById("RadioParceiro1").checked,
    RadioParceiro2: document.getElementById("RadioParceiro2").checked,
    parceiro: $('#parceiro').val().toString(),
    RadioRevisao1: document.getElementById("RadioRevisao1").checked,
    RadioRevisao2: document.getElementById("RadioRevisao2").checked,
    revisao: $('#revisao').val().toString(),
    RadioComplexidade1: document.getElementById("RadioComplexidade1").checked,
    RadioComplexidade2: document.getElementById("RadioComplexidade2").checked,
    complexidade: $('#complexidade').val().toString(),
    RadioEvento1: document.getElementById("RadioEvento1").checked,
    RadioEvento2: document.getElementById("RadioEvento2").checked,
    evento: $('#evento').val().toString(),
    Composicao: document.getElementById("Composicao").checked,
    PR: document.getElementById("PR").checked,
    NMAP: document.getElementById("NMAP").checked,
    NA: document.getElementById("NA").checked,
    resultado: document.getElementById("resultado").checked,
    sustentabilidade: document.getElementById("sustentabilidade").checked,
    governanca: document.getElementById("governanca").checked,
    quebra: document.getElementById("quebra").checked,
    confidencialidade: document.getElementById("confidencialidade").checked,
    ambiental: document.getElementById("ambiental").checked,
    health: document.getElementById("health").checked,
    reputacao: document.getElementById("reputacao").checked,
    violacao: document.getElementById("violacao").checked,
    auditoria: document.getElementById("auditoria").checked,
    trabalhista: document.getElementById("trabalhista").checked,
    outros: document.getElementById("outros").checked,
    oevento: $('#oevento').val().toString(),
    causa: $('#causa').val().toString(),
    impacto: $('#impacto').val().toString(),
    mesma: $('#mesma').val().toString(),
    diferente: $('#diferente').val().toString(),
    sabemos: $('#sabemos').val().toString(),
    melhorar: $('#melhorar').val().toString(),
    atual: $('#atual').val().toString(),
    Elaborador: $('#Elaborador').val().toString(),
    At1: $('#At1').val() +"@arcadis.com",
    At2: $('#At2').val()+"@arcadis.com",
    At3: $('#At3').val()+"@arcadis.com",
    PMO1: $('#pmo1').val()+"@arcadis.com",
    PMO2: $('#pmo2').val()+"@arcadis.com",
    PMO3: $('#pmo3').val()+"@arcadis.com",
    acervo_tecnico: $('#acervo_tecnico').val()+"@arcadis.com",
    qualidade: $('#qualidade').val()+"@arcadis.com",
    comercial: $('#comercial').val()+"@arcadis.com",
    sso: $('#sso').val()+"@arcadis.com",
    controladoria: $('#controladoria').val()+"@arcadis.com",
    opcional1: $('#opcional1').val()+"@arcadis.com",
    opcional2: $('#opcional2').val()+"@arcadis.com",
    opcional3: $('#opcional3').val()+"@arcadis.com",
    mes1: $('#mes1').val().toString(),
    mes2: $('#mes2').val().toString(),
    mes3: $('#mes3').val().toString(),
    mes4: $('#mes4').val().toString(),
    mes5: $('#mes5').val().toString(),
    mes6: $('#mes6').val().toString(),
    mes7: $('#mes7').val().toString(),
    mes8: $('#mes8').val().toString(),
    mes9: $('#mes9').val().toString(),
    mes10: $('#mes10').val().toString(),
    mes11: $('#mes11').val().toString(),
    mes12: $('#mes12').val().toString(),
    media: $('#media').val().toString(),
    premio: $('#premiacao').val().toString(),
    reuniao: Reunião,
  };

  var objeto = JSON.stringify(obj);

 

  var xmlhttpFinish = new XMLHttpRequest();
  var urlgeral = 'https://prod-89.westus.logic.azure.com:443/workflows/f69da11c19e648499f3f03fde2a5c861/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=kT95B0YHue3HQSB7Mc3ourN3HTUql_jaMfeU1yaezl0';

  xmlhttpFinish.open("POST", urlgeral, true);
  xmlhttpFinish.setRequestHeader('Content-Type', 'application/json');

  xmlhttpFinish.onload = function () {
  
    if (xmlhttpFinish.readyState === xmlhttpFinish.DONE) {
        if (xmlhttpFinish.status === 202) {
          window.open('https://arcadiso365.sharepoint.com/teams/PortalAtadefechamento/Documentos%20Compartilhados/Forms/AllItems.aspx', '_blank');
        }
      }
      document.getElementById("carregando").hidden = true;
      document.getElementById("mensagem").innerHTML = "";
    }
    debugger
    xmlhttpFinish.send(objeto); 
    xmlhttpFinish.responseType = 'text';
});

/////////////////////////////Recuperar todos os CCs e campos fixos///////////////////////////////////
var urlcc = "https://prod-128.westus.logic.azure.com:443/workflows/c8cec5ed208c42d4aeed05daf9fa2e68/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=XvXr2UicIjeRv8vCMsbqGpRjEWSx1BVVi6H60ZBLJdY";
var urlservico = "https://prod-44.westus.logic.azure.com:443/workflows/9ac2bf309d154c46bfb8c8e3158107b7/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=X5NNUCOUfAL-d5pMyr26jfbVrONDhAmykvpgW_-8w04";
var ccs = [];
var servicos =[];
var xmlhttpCC = new XMLHttpRequest();
var xmlhttpservico = new XMLHttpRequest();
var html = "";
var service = "";
xmlhttpCC.open("POST", urlcc, true);
xmlhttpCC.setRequestHeader('Content-Type', 'application/json');
xmlhttpservico.open("POST", urlservico, true);
xmlhttpservico.setRequestHeader('Content-Type', 'application/json');

document.getElementById("mensagem").innerHTML ="Carregando Centros de Custo...";
document.getElementById("carregando").hidden = false;

xmlhttpCC.onload = function () {
  
  if (xmlhttpCC.readyState === xmlhttpCC.DONE) {
      if (xmlhttpCC.status === 200) {
        ccs = JSON.parse(xmlhttpCC.responseText);
        for(var key in ccs) {
          html += "<option value=" + ccs[key].CC + ">";
        }
        
        document.getElementById("CC_option").innerHTML = html;
        
      }
    }
    document.getElementById("carregando").hidden = true;
    document.getElementById("mensagem").innerHTML = "";
};

//tipo de serviço///
xmlhttpservico.onload = function () {
  
  document.getElementById("mensagem").innerHTML ="Carregando serviços...";
  document.getElementById("carregando").hidden = false;
  if (xmlhttpservico.readyState === xmlhttpservico.DONE) {
    if (xmlhttpservico.status === 200) {             
        servicos = JSON.parse(xmlhttpservico.responseText);
        for(var key in servicos) {
          service += "<option value='" + servicos[key].TipoDeServico + "'>";
        }
      document.getElementById("service_option").innerHTML = service;
    }
  }
  document.getElementById("carregando").hidden = true;
  document.getElementById("mensagem").innerHTML = "";
}

xmlhttpCC.send(); 
xmlhttpCC.responseType = 'text';
xmlhttpservico.send(); 
xmlhttpservico.responseType = 'text';

////meses dro
/*var meses = ["jan", "fev", "mar", "abr", "mai", "jun", "jul", "ago", "set", "out", "nov", "dec"];
var ano = data.getFullYear().toString().substr(-2);
var mes = data.getMonth();
var mesesformatados = [];
var caracter = "/";
var l;
for (l=0; l<12;l++){
  mesesformatados[l] = meses[mes-l] + caracter + ano;
  if(mes-l == 0){
    mes = 12+l;
    ano--;
    caracter="-";
  }
}
  /*document.getElementById("jan").innerHTML = mesesformatados[11];
  document.getElementById("fev").innerHTML = mesesformatados[10];
  document.getElementById("mar").innerHTML = mesesformatados[9];
  document.getElementById("abr").innerHTML = mesesformatados[8];
  document.getElementById("mai").innerHTML = mesesformatados[7];
  document.getElementById("jun").innerHTML = mesesformatados[6];
  document.getElementById("jul").innerHTML = mesesformatados[5];
  document.getElementById("ago").innerHTML = mesesformatados[4];
  document.getElementById("set").innerHTML = mesesformatados[3];
  document.getElementById("out").innerHTML = mesesformatados[2];
  document.getElementById("nov").innerHTML = mesesformatados[1];
  document.getElementById("dec").innerHTML = mesesformatados[0];*/

  //////////////////Filtrar infos de acordo com CC //////////////////////////q
document.getElementById("CC").addEventListener("change", function(){
  document.getElementById("carregando").hidden = false;
  document.getElementById("mensagem").innerHTML ="Carregando informações correspondentes ao CC: "+ document.getElementById("CC").value;

  //Zerar infos
  document.getElementById("Elaboracao").value="";
  document.getElementById("PMO").value="";
  document.getElementById("Proposta").value="";
  document.getElementById("CNPJ").value="";
  document.getElementById("LM_just").value="";
  document.getElementById("venda_ho_just").value="";
  document.getElementById("ho_target_just").value="";
  document.getElementById("target_encerramento_just").value="";
  document.getElementById("RadioEncerramento2").checked=true;
  document.getElementById("Acoes_encerramento").value="";
  document.getElementById("responsavel_encerramento").value="";
  document.getElementById("prazo_encerramento").value="";
  document.getElementById("RadioJuridico2").checked=true;
  document.getElementById("Acoes_juridico").value="";
  document.getElementById("responsavel_juridico").value="";
  document.getElementById("prazo_juridico").value="";
  document.getElementById("RadioInconsistencia2").checked=true;
  document.getElementById("Acoes_ativo").value="";
  document.getElementById("responsavel_ativo").value="";
  document.getElementById("prazo_ativo").value="";
  document.getElementById("RadioRecurso2").checked=true;
  document.getElementById("Acoes_recurso").value="";
  document.getElementById("responsavel_recurso").value="";
  document.getElementById("prazo_recurso").value="";
  document.getElementById("RadioRateio2").checked=true;
  document.getElementById("Acoes_rateio").value="";
  document.getElementById("responsavel_rateio").value="";
  document.getElementById("prazo_rateio").value="";
  document.getElementById("RadioFixo2").checked=true;
  document.getElementById("Acoes_fixo").value="";
  document.getElementById("responsavel_fixo").value="";
  document.getElementById("prazo_fixo").value="";
  document.getElementById("RadioConsorcio2").checked=true;
  document.getElementById("RadioLider1").checked=false;
  document.getElementById("RadioLider2").checked=false;
  document.getElementById("RadioDistrato1").checked=false;
  document.getElementById("RadioDistrato2").checked=false;
  document.getElementById("RadioQuitacao1").checked=false;
  document.getElementById("RadioQuitacao2").checked=false;
  document.getElementById("RadioBaixa1").checked=false;
  document.getElementById("RadioBaixa2").checked=false;
  document.getElementById("Acoes_consorcio").value="";
  document.getElementById("responsavel_consorcio").value="";
  document.getElementById("prazo_consorcio").value="";
  document.getElementById("aspectos").value="";
  document.getElementById("RadioSatisfacao2").checked=true;
  document.getElementById("satisfacao_just").value="";
  document.getElementById("satisfacao_nome").value="";
  document.getElementById("satisfacao_contato").value="";
  document.getElementById("registros").value="";
  document.getElementById("aditivos").value="";
  document.getElementById("local").value="";
  document.getElementById("responsavel_local").value="";
  document.getElementById("local_prazo").value="";
  document.getElementById("gng_ho").value="";
  document.getElementById("project_review").value="";
  document.getElementById("prazo_realizado").value="";
  document.getElementById("custo_efetivo").value="";
  document.getElementById("alteracao_requisito").value="";
  document.getElementById("desvios_evitados").value="";
  document.getElementById("comunicacao").value="";
  document.getElementById("adm").value="";
  document.getElementById("entrega").value="";
  document.getElementById("capacidade").value="";
  document.getElementById("parceiro").value="";
  document.getElementById("revisao").value="";
  document.getElementById("complexidade").value="";
  document.getElementById("evento").value="";
  document.getElementById("RadioPrazo2").checked=true;
  document.getElementById("RadioCusto2").checked=true;
  document.getElementById("RadioAlteracao2").checked=true;
  document.getElementById("RadioDesvio2").checked=true;
  document.getElementById("RadioComunicacao2").checked=true;
  document.getElementById("RadioAdm2").checked=true;
  document.getElementById("RadioParceiro2").checked=true;
  document.getElementById("RadioEntrega2").checked=true;
  document.getElementById("RadioCapacidade2").checked=true;
  document.getElementById("RadioEvento2").checked=true;
  document.getElementById("RadioComplexidade2").checked=true;
  document.getElementById("RadioRevisao2").checked=true;
  document.getElementById("NA").checked=true;
  document.getElementById("resultado").checked=false;
  document.getElementById("sustentabilidade").checked=false;
  document.getElementById("governanca").checked=false;
  document.getElementById("quebra").checked=false;
  document.getElementById("confidencialidade").checked=false;
  document.getElementById("ambiental").checked=false;
  document.getElementById("health").checked=false;
  document.getElementById("reputacao").checked=false;
  document.getElementById("violacao").checked=false;
  document.getElementById("auditoria").checked=false;
  document.getElementById("trabalhista").checked=false;
  document.getElementById("outros").checked=false;
  document.getElementById("oevento").value="";
  document.getElementById("causa").value="";
  document.getElementById("impacto").value="";
  document.getElementById("mesma").value="";
  document.getElementById("diferente").value="";
  document.getElementById("sabemos").value="";
  document.getElementById("melhorar").value="";
  document.getElementById("Elaborador").value="";

  var urlProjetoAno = "https://prod-163.westus.logic.azure.com:443/workflows/c59c4707f8254a8981627a6cca7c4acb/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=Yh8zpv98Ucik1mTsl-XS552Eo24qFYAJ1qhAIbBKsiU";
  var urlconsorcio = "https://prod-155.westus.logic.azure.com:443/workflows/f333c4a732cd48d492d23f7321dc6394/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=qGFR7eMALIb3aQfZAqTLBnLX9pWauObS48QrwCcMrmw";
  var urllpr = "https://prod-41.westus.logic.azure.com:443/workflows/65842b5a1d5347acb7dbd8cdd7ed8bd2/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=-tycfPvouFDrK9YZIF9_6X33mYkH-hKBJdbi9BxBSUw";
  //var urldro = "https://prod-119.westus.logic.azure.com:443/workflows/b0455a89df5f455d8737338476702f5d/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=s05vlckUtO2uUgWBTrOdJFOS18JtfJQ_IIuqt_UkMFI";
  var urlOneLiner = "https://prod-127.westus.logic.azure.com:443/workflows/51478d19db1d44bca6507b82f9777e6e/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=d81wO54wvlFcECQxIIVoVzqaB_TLi0_HtI_higkwLHM";
  var urlArt = "https://prod-56.westus.logic.azure.com:443/workflows/6c5e7f04db20474fbbf9679c19e87f9e/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=IPnm_yTY6lG9DuouIiQPZ5U1TonJ3fJFfQhqI-4B4xo";
  var urlRoot1 = "https://prod-184.westus.logic.azure.com:443/workflows/aa25f2d2d91947809db77ff8c4b4fd5c/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=rgp-oRxh3aOMaJEtE33PI6Ww5sKaaKJ82FwA7KS6K6A";

  var dados = [];
  var consorcio = [];
  var art = [];
  var root1 = [];
  var mepc1=[];
  var mepc2=[];

  var xmlhttpArt = new XMLHttpRequest();
  var xmlhttpOneLiner = new XMLHttpRequest();
  var xmlhttpLPR = new XMLHttpRequest();
  var xmlhttpDRO = new XMLHttpRequest();
  var xmlhttpProjetoAno = new XMLHttpRequest();
  var xmlhttpConsorcio = new XMLHttpRequest();
  var xmlhttpRoot1 = new XMLHttpRequest();

  const json = {"CC": $('#CC').val()};
  
  xmlhttpOneLiner.open("POST", urlOneLiner, true);
  xmlhttpOneLiner.setRequestHeader('Content-Type', 'application/json');
  xmlhttpProjetoAno.open("POST", urlProjetoAno, true);
  xmlhttpProjetoAno.setRequestHeader('Content-Type', 'application/json');
  xmlhttpLPR.open("POST", urllpr, true);
  xmlhttpLPR.setRequestHeader('Content-Type', 'application/json');
  xmlhttpConsorcio.open("POST", urlconsorcio, true);
  xmlhttpConsorcio.setRequestHeader('Content-Type', 'application/json');
  xmlhttpArt.open("POST", urlArt, true);
  xmlhttpArt.setRequestHeader('Content-Type', 'application/json');
  xmlhttpRoot1.open("POST", urlRoot1, true);
  xmlhttpRoot1.setRequestHeader('Content-Type', 'application/json');
  //xmlhttpDRO.open("POST", urldro, true);
  //xmlhttpDRO.setRequestHeader('Content-Type', 'application/json');

  xmlhttpOneLiner.onload = function () {

    document.getElementById("mensagem").innerHTML ="Carregando informações do OneLiner...";
    document.getElementById("carregando").hidden = false;
  
    if (xmlhttpOneLiner.readyState === xmlhttpOneLiner.DONE) {
      if (xmlhttpOneLiner.status === 200) {

        document.getElementById("carregando").hidden = true;
        dados = JSON.parse(xmlhttpOneLiner.responseText);
      
        //Gerenciamento de data inicial e final
        var date2 = new Date();
        var begin = moment(dados.Inicio.substring(0,10), "YYYY/MM/DD");
        if (dados.Termino !=""){
          var end = moment(dados.Termino.substring(0,10), "YYYY/MM/DD")
          date2 = end;
        }else {
          var end = "";
          date2 = begin;
        };
        const date1 = new Date(begin);

        //Alimentação do form Com infos do oneliner
        document.getElementById("Contrato").value = dados.Contrato;
        document.getElementById("Grupo").value = dados.Grupo;
        document.getElementById("Diretor").value = dados.Diretor;
        document.getElementById("Gerente").value = dados.Gerente;
        document.getElementById("Coordenador").value = dados.Coordenador;
        document.getElementById("Controller").value = dados.Controller;
        document.getElementById("Inicio").value = begin.format('DD/MM/YYYY');
        document.getElementById("Termino").value = end != "" ? end.format('DD/MM/YYYY') : "";
        document.getElementById("CC_Nome").value = dados.Projeto;
        document.getElementById("Tipo_Receita").value = dados.Tipo;
        document.getElementById("BE").value = dados.BE != "" ? dados.BE : 0;
        document.getElementById("BE").innerHTML = parseFloat(document.getElementById("BE").value*100).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2})+"%";
        document.getElementById("Venda").value = dados.Venda != "" ? dados.Venda : 0;
        document.getElementById("Venda").innerHTML = parseFloat(document.getElementById("Venda").value*100).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2})+"%";
        document.getElementById("Valor_total_handover").value = dados.HValorTotal != "" ? dados.HValorTotal : 0;
        document.getElementById("Valor_total_handover").innerHTML =parseFloat(document.getElementById("Valor_total_handover").value).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
        document.getElementById("Gross_revenue_handover").value = dados.HGrossRevenue != "" ? dados.HGrossRevenue : 0;
        document.getElementById("Gross_revenue_handover").innerHTML = parseFloat(document.getElementById("Gross_revenue_handover").value).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
        document.getElementById("Net_revenue_handover").value = dados.HNetRev != "" ? dados.HNetRev : 0;
        document.getElementById("Net_revenue_handover").innerHTML = parseFloat(document.getElementById("Net_revenue_handover").value).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
        document.getElementById("gm-net_handover").value = dados.Hgm != "" ? dados.Hgm : 0;
        document.getElementById("gm-net_handover").innerHTML = parseFloat(document.getElementById("gm-net_handover").value*100).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2})+"%";
        document.getElementById("gross_margin_handover").value = dados.HGrossmargin != "" ? dados.HGrossmargin : 0;
        document.getElementById("gross_margin_handover").innerHTML = parseFloat(document.getElementById("gross_margin_handover").value).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
        document.getElementById("margem_bruta_handover").value = dados.HMargemBruta != "" ? dados.HMargemBruta : 0;
        document.getElementById("margem_bruta_handover").innerHTML = parseFloat(document.getElementById("margem_bruta_handover").value*100).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2})+"%";
        document.getElementById("Valor_total_target").value = dados.TValorTotal != "" ? dados.TValorTotal : 0;
        document.getElementById("Valor_total_target").innerHTML = parseFloat(document.getElementById("Valor_total_target").value).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
        document.getElementById("Gross_revenue_target").value = dados.TGrossRevenue != "" ? dados.TGrossRevenue : 0;
        document.getElementById("Gross_revenue_target").innerHTML = parseFloat(document.getElementById("Gross_revenue_target").value).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
        document.getElementById("Net_revenue_target").value = dados.TNetRev != "" ? dados.TNetRev : 0;
        document.getElementById("Net_revenue_target").innerHTML = parseFloat(document.getElementById("Net_revenue_target").value).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
        document.getElementById("gm-net_target").value = dados.Tgm != "" ? dados.Tgm : 0;
        document.getElementById("gm-net_target").innerHTML = parseFloat(document.getElementById("gm-net_target").value*100).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2})+"%";;
        document.getElementById("gross_margin_target").value = dados.TGrossmargin != "" ? dados.TGrossmargin : 0;
        document.getElementById("gross_margin_target").innerHTML = parseFloat(document.getElementById("gross_margin_target").value).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
        document.getElementById("margem_bruta_target").value = dados.TMargemBruta != "" ? dados.TMargemBruta : 0;
        document.getElementById("margem_bruta_target").innerHTML = parseFloat(document.getElementById("margem_bruta_target").value*100).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2})+"%";
        document.getElementById("Valor_total_encerramento").value = dados.EValorTotal != "" ? dados.EValorTotal : 0;
        document.getElementById("Valor_total_encerramento").innerHTML = parseFloat(document.getElementById("Valor_total_encerramento").value).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
        document.getElementById("Gross_revenue_encerramento").value = dados.EGrossRevenue != "" ? dados.EGrossRevenue : 0;
        document.getElementById("Gross_revenue_encerramento").innerHTML = parseFloat(document.getElementById("Gross_revenue_encerramento").value).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
        document.getElementById("Net_revenue_encerramento").value = dados.ENetRev != "" ? dados.ENetRev : 0;
        document.getElementById("Net_revenue_encerramento").innerHTML = parseFloat(document.getElementById("Net_revenue_encerramento").value).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
        document.getElementById("gm-net_encerramento").value = dados.Egm != "" ? dados.Egm : 0;
        document.getElementById("gm-net_encerramento").innerHTML = parseFloat(document.getElementById("gm-net_encerramento").value*100).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2})+"%";;
        document.getElementById("gross_margin_encerramento").value = dados.EGrossmargin != "" ? dados.EGrossmargin : 0;
        document.getElementById("gross_margin_encerramento").innerHTML = parseFloat(document.getElementById("gross_margin_encerramento").value).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
        document.getElementById("margem_bruta_encerramento").value = dados.EMargemBruta != "" ? dados.EMargemBruta : 0;
        document.getElementById("margem_bruta_encerramento").innerHTML = parseFloat(document.getElementById("margem_bruta_encerramento").value*100).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2})+"%";
        document.getElementById("categoria_encerramento").value = dados.AwayEncerramento;
        document.getElementById("categoria_encerramento").innerHTML = document.getElementById("categoria_encerramento").value;
        document.getElementById("servico_fatura").value = ((isNaN(dados.ServicoFaturar)) || dados.ServicoFaturar == 0) ? "0" : (dados.ServicoFaturar).toLocaleString(undefined,  { minimumFractionDigits: 2,maximumFractionDigits: 2 } );
        document.getElementById("servico_receber").value = ((isNaN(dados.FaturaReceber)) || dados.FaturaReceber==0) ? "0" : (dados.FaturaReceber).toLocaleString(undefined,  { minimumFractionDigits: 2,maximumFractionDigits: 2 } );
        document.getElementById("causa_retencao").value = ((isNaN(dados.Caucao)) || dados.Caucao==0)  ? "0" : (dados.Caucao).toLocaleString(undefined,  { minimumFractionDigits: 2,maximumFractionDigits: 2 } );
        document.getElementById("profit").value = isNaN(document.getElementById("Net_revenue_encerramento").value * (document.getElementById("gm-net_encerramento").value - document.getElementById("gm-net_target").value)) ? "" : document.getElementById("Net_revenue_encerramento").value * (document.getElementById("gm-net_encerramento").value - document.getElementById("gm-net_target").value);
        document.getElementById("profit").innerHTML =  document.getElementById("profit").value.toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
        document.getElementById("backlog").value = ((isNaN(dados.Adiantamento + dados.Backlog)) || (dados.Adiantamento + dados.Backlog)==0) ? "0" : (parseFloat(dados.Adiantamento) + parseFloat(dados.Backlog)).toLocaleString('pt-BR',  { minimumFractionDigits: 2,maximumFractionDigits: 2 } );
        //document.getElementById("dro_target").value = dados.DROTarget;
        //document.getElementById("dro_target").innerHTML = dados.DROTarget;
        document.getElementById("Acoes_juridico").value = ((isNaN(dados.Juridico)) || dados.Juridico ==0) ? "0" : "PDD: " + dados.Juridico.toLocaleString('pt-BR',  { minimumFractionDigits: 2,maximumFractionDigits: 2 } );
        document.getElementById("categoria_encerramento").value = dados.Projeto;
        document.getElementById("LM").value = isNaN(dados.Venda - dados.BE) ? "": ((dados.Venda - dados.BE)*100).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2})+"%";
        document.getElementById("target_encerramento").value = isNaN(dados.Egm - dados.BE) ? "" : ((dados.Egm - dados.BE)*100).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2})+"%";
        document.getElementById("Venda_ho").value =isNaN(dados.Hgm - dados.Venda) ? "" : ((dados.Hgm - dados.Venda)*100).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2})+"%";
        document.getElementById("ho_target").value =isNaN(dados.Tgm - dados.Hgm) ? "" : ((dados.Tgm - dados.Hgm)*100).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2})+"%";

        //Manejo de data
        const diffTime = Math.abs(date2 - date1);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
        document.getElementById("duracao_encerramento").value =   dados.Termino!="" ?  ((diffDays)/30).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2}) : "Data inválida";
        document.getElementById("duracao_encerramento").innerHTML = document.getElementById("duracao_encerramento").value;
        document.getElementById("duracao_variacao").value =  document.getElementById("duracao_handover").value!=undefined ?  (document.getElementById("duracao_encerramento").value)-(document.getElementById("duracao_handover").value).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2}) : "Sem Handover";
        document.getElementById("duracao_variacao").innerHTML = document.getElementById("duracao_variacao").value;
        
        //Variacao              
        variacao("Valor_total_target","Valor_total_encerramento","Valor_total_variacao");
        variacao("Gross_revenue_target","Gross_revenue_encerramento","Gross_revenue_variacao");
        variacao("Net_revenue_target","Net_revenue_encerramento","Net_revenue_variacao");
        variacao("gm-net_target","gm-net_encerramento","gm-net_variacao");
        variacao("gross_margin_target","gross_margin_encerramento","gross_margin_variacao");
        variacao("margem_bruta_target","margem_bruta_encerramento","margem_bruta_variacao");
        document.getElementById("margem_bruta_variacao").innerHTML = parseFloat(document.getElementById("margem_bruta_variacao").value*100).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2})+"%";
        document.getElementById("gm-net_variacao").innerHTML = parseFloat(document.getElementById("gm-net_variacao").value*100).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2})+"%";

        //Formatação de cards sim/não
        if (!(isNaN(document.getElementById("profit").value))){
          if (document.getElementById("profit").value <0){
            document.getElementById("profit").style.backgroundColor = "red";
          }
          else{document.getElementById("profit").style.backgroundColor = "blue";}
        }
        
        if (document.getElementById("servico_fatura").value != "0"){
          document.getElementById("RadioServico1").checked=true;
          document.getElementById("RadioServico2").checked=false;
        }
        else{
          document.getElementById("RadioServico1").checked=false;
          document.getElementById("RadioServico2").checked=true;
        }

        if (document.getElementById("servico_receber").value != "0"){
          document.getElementById("RadioFatura1").checked=true;
          document.getElementById("RadioFatura2").checked=false;
        }
        else{
          document.getElementById("RadioFatura1").checked=false;
          document.getElementById("RadioFatura2").checked=true;
        }

        if (document.getElementById("causa_retencao").value != "0"){
          document.getElementById("RadioRetencao1").checked=true;
          document.getElementById("RadioRetencao2").checked=false;
        }
        else{
          document.getElementById("RadioRetencao1").checked=false;
          document.getElementById("RadioRetencao2").checked=true;
        }

        if (document.getElementById("backlog").value != "0"){
          document.getElementById("RadioBack1").checked=true;
          document.getElementById("RadioBack2").checked=false;
        }
        else{
          document.getElementById("RadioBack1").checked=false;
          document.getElementById("RadioBack2").checked=true;
        }
              
        if (document.getElementById("Valor_total_variacao").value < "0"){
          
          document.getElementById("cancelamento").value = document.getElementById("Valor_total_variacao").innerHTML.toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
          document.getElementById("RadioCancelamento1").checked=true;
          document.getElementById("RadioCancelamento2").checked=false;
        }
        else{
          document.getElementById("cancelamento").value = 0;
          document.getElementById("RadioCancelamento1").checked=false;
          document.getElementById("RadioCancelamento2").checked=true;
        }
        if (document.getElementById("Acoes_juridico").value != "0"){
          document.getElementById("RadioJuridico2").checked=false;
          document.getElementById("RadioJuridico1").checked=true;
        }

        //Tipo de servico reset///
        document.getElementById("Tipo_servico").value = "";

        const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
        })

        var premissa = "Necessita reunião: Valor do contrato > 250.000 <br> Categoria: Amber/Red <br> Margem < HO <br>" + " <br> <h3>Informações do CC:</h3> Valor do contrato: "+ document.getElementById("Valor_total_encerramento").innerHTML + " <br> Categoria: " + dados.AwayEncerramento +" <br> Margem Net: " + document.getElementById("gm-net_encerramento").innerHTML+  " <br> Margen HO: " + document.getElementById("gm-net_handover").innerHTML + " <br> <br> <h4>Necessita de reunião?</h4>";
        
        swalWithBootstrapButtons.fire({
          title: 'Premissas para reunião',
          html: premissa,
          icon: 'info',
          showCancelButton: true,
          allowOutsideClick: false,
          allowEscapeKey: false,
          allowEnterKey: false,
          confirmButtonText: 'Sim',
          cancelButtonText: 'Não',
          reverseButtons: true
        }).then((result) => {
          if (result.isConfirmed) {
            swalWithBootstrapButtons.fire(
              'Registrado!',
              'É necessário marcar reunião',
              'success'
            )
            Reunião  ="Sim";
          } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
          ) {
            swalWithBootstrapButtons.fire(
              'Registrado!',
              'Não é necessário marcar reunião',
              'success'
            )
            Reunião  ="Não";
          }
        })


      } else{
        document.getElementById("carregando").hidden = true;
        alert("Nenhum Centro de Custo foi encontrado com o valor: " + document.getElementById("CC").value);
        document.getElementById("CC").value="";

      }
    }
          document.getElementById("carregando").hidden = true;
          document.getElementById("mensagem").innerHTML = "";
  };

  //Projeto do Ano//
  xmlhttpProjetoAno.onload = function () {
    document.getElementById("mensagem").innerHTML ="Projeto do ano...";
    document.getElementById("carregando").hidden = false;
    if (xmlhttpProjetoAno.readyState === xmlhttpProjetoAno.DONE) {
      if (xmlhttpProjetoAno.status === 200) {
        document.getElementById("premiacao").innerHTML= '"' + xmlhttpProjetoAno.responseText + '"';
        document.getElementById("Projeto_ano").hidden=false;
      }
    }
    document.getElementById("carregando").hidden = true;
    document.getElementById("mensagem").innerHTML = "";
  }

  //////3.7 (1)//////
  xmlhttpRoot1.onload = function () {
    
    document.getElementById("mensagem").innerHTML ="Causa Raiz...";
    document.getElementById("carregando").hidden = false;

    if (xmlhttpRoot1.readyState === xmlhttpRoot1.DONE) {
      if (xmlhttpRoot1.status === 200) {
        
        root1= JSON.parse(xmlhttpRoot1.responseText);

        if(root1.length>1){
          for(var key in root1) {
            mepc1 += "<option value='" + root1[key].Title + "'>";
          }
          document.getElementById("root1").innerHTML = mepc1;
          document.getElementById("root2").innerHTML = mepc2;
          document.getElementById("analise1").value = "";
          document.getElementById("analise2").value = "";
          document.getElementById("analise3").value = ""; 
        }else{
          document.getElementById("analise1").value = root1.Root1;
          document.getElementById("analise2").value = root1.Root2;
          document.getElementById("analise3").value = root1.Root3;  
        }
      }
    }
    document.getElementById("carregando").hidden = true;
    document.getElementById("mensagem").innerHTML = "";
  }

/////Preencher 3.7(2) e carregar 3.7 (3)////////
document.getElementById("analise1").addEventListener("change", function(){

  for(var key in root1) {
    if (document.getElementById("analise1").value ==  root1[key].Title ) {
      document.getElementById("analise2").value = root1[key].Descri_x00e7__x00e3_odacategoria;
      var posicao = key;
      var xmlhttpRoot3 = new XMLHttpRequest();
      var urlroot3 ="https://prod-164.westus.logic.azure.com:443/workflows/9626784a9df845848f8263c201650930/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=-FbUb9CfRbMi3mezmxNxuyp_4D91euMk-TfGECsBuKk";
      var root3 = [];
      var mepc3="";
      const indice_rootCause = {"Indice": "0"};
      
      xmlhttpRoot3.open("POST", urlroot3, true);
      xmlhttpRoot3.setRequestHeader('Content-Type', 'application/json');
      xmlhttpRoot3.onload = function () {
        if (xmlhttpRoot3.readyState === xmlhttpRoot3.DONE) {
          if (xmlhttpRoot3.status === 200) {
            root3= JSON.parse(xmlhttpRoot3.responseText);
            var indice = root1[posicao].Indice;
            for(var key in root1) {
              if (root3[key]["\""+indice+"\""]!==null){
                mepc3 += "<option value='" + root3[key]["\""+indice+"\""] + "'>";
              }
            }
            document.getElementById("root3").innerHTML = mepc3;
          }
        }
      }
      xmlhttpRoot3.send(JSON.stringify(indice_rootCause)); 
      xmlhttpRoot3.responseType = 'text';
      break;
    }
  }
});

  //4.4Consorcio////////
  xmlhttpConsorcio.onload = function () {
    
    document.getElementById("mensagem").innerHTML ="Carregando consórcios...";
    document.getElementById("carregando").hidden = false;

    if (xmlhttpConsorcio.readyState === xmlhttpConsorcio.DONE) {
      if (xmlhttpConsorcio.status === 200) {
        document.getElementById("RadioConsorcio1").checked=true;
        document.getElementById("RadioConsorcio2").checked=false;
        consorcio =  JSON.parse(xmlhttpConsorcio.responseText);
        document.getElementById("Acoes_consorcio").value = document.getElementById("Acoes_consorcio").value + " " + consorcio.Observacao;
        document.getElementById("EC").innerHTML = "Saldo do Encontro de Contas: " + consorcio.Posicao;
        if (consorcio.Posicao <0){
          document.getElementById("EC").style.color = "red";
        }
        else{ document.getElementById("EC").style.color = "green";}
      }
      else{
        document.getElementById("RadioConsorcio2").checked=true;
        document.getElementById("RadioConsorcio1").checked=false;
        document.getElementById("Acoes_consorcio").value ="";
        document.getElementById("responsavel_consorcio").value = "";
        document.getElementById("prazo_consorcio").value = "";
      }
    }
    document.getElementById("carregando").hidden = true;
    document.getElementById("mensagem").innerHTML = "";
  }

 ////8.lições aprendidas///////
  xmlhttpLPR.onload = function () {
    document.getElementById("mensagem").innerHTML ="Carregando LPR...";
    document.getElementById("carregando").hidden = false;
    if (xmlhttpLPR.readyState === xmlhttpLPR.DONE) {
        if (xmlhttpLPR.status === 200) {
         document.getElementById("licoes_aprendidas").value= '"' + xmlhttpLPR.responseText + '"';
        }else{document.getElementById("licoes_aprendidas").value=""}
    }else{document.getElementById("licoes_aprendidas").value=""}
    document.getElementById("carregando").hidden = true;
    document.getElementById("mensagem").innerHTML = "";
  }

  //////6. Documentação //////////////
  xmlhttpArt.onload = function () {
    var l=0;
    document.getElementById("mensagem").innerHTML ="Carregando ART...";
    document.getElementById("carregando").hidden = false;
    if (xmlhttpArt.readyState === xmlhttpArt.DONE) {
      if (xmlhttpArt.status === 200) {
        art = JSON.parse(xmlhttpArt.responseText);
        for (l=0; l<art.value.length;l++){
          debugger
          if(art.value[l].TipodeDocumento.Value == "Atestado Parcial"){
            document.getElementById("acoes_atestado").value = document.getElementById("acoes_atestado").value + " Atestado Parcial Assinado: "+ art.value[l].Assinado.Value +"\n";
            document.getElementById("RadioAtestado1").checked = true;
            document.getElementById("local").value = "https://arcadiso365.sharepoint.com/sites/CorporativoBR/acervotecnico/Sistema%20Acervo/Forms/Base2.aspx";

          } else if(art.value[l].TipodeDocumento.Value == "Atestado Final"){
            document.getElementById("acoes_atestado").value = document.getElementById("acoes_atestado").value + " Atestado Final Assinado: "+ art.value[l].Assinado.Value+"\n";
            document.getElementById("RadioAtestado1").checked = true;
            document.getElementById("local").value = "https://arcadiso365.sharepoint.com/sites/CorporativoBR/acervotecnico/Sistema%20Acervo/Forms/Base2.aspx";
            
          } else if(art.value[l].TipodeDocumento.Value == "ART"){
            document.getElementById("Acoes_art").value = document.getElementById("Acoes_art").value + " ART Assinada: "+ art.value[l].Assinado.Value+"\n";
            document.getElementById("RadioArt1").checked = true;
            document.getElementById("local").value = "https://arcadiso365.sharepoint.com/sites/CorporativoBR/acervotecnico/Sistema%20Acervo/Forms/Base2.aspx";

          } else if(art.value[l].TipodeDocumento.Value == "Contrato"){
            document.getElementById("Acoes_contrato").value = document.getElementById("Acoes_contrato").value + " Contrato Assinado: "+ art.value[l].Assinado.Value+"\n";
            document.getElementById("RadioArquivado1").checked = true;
            document.getElementById("local").value = "https://arcadiso365.sharepoint.com/sites/CorporativoBR/acervotecnico/Sistema%20Acervo/Forms/Base2.aspx";
            
          } else if(art.value[l].TipodeDocumento.Value == "Aditivos"){
            document.getElementById("Acoes_contrato").value = document.getElementById("Acoes_contrato").value + " Aditivo Assinado: "+ art.value[l].Assinado.Value+"\n";
            document.getElementById("RadioArquivado1").checked = true;
            document.getElementById("local").value = "https://arcadiso365.sharepoint.com/sites/CorporativoBR/acervotecnico/Sistema%20Acervo/Forms/Base2.aspx";
            
          } else if(art.value[l].TipodeDocumento.Value == "Consorcio"){
            document.getElementById("Acoes_consorcio").value = document.getElementById("Acoes_consorcio").value + " Consórcio Assinado: "+ art.value[l].Assinado.Value+"\n";
            document.getElementById("RadioConsorcio1").checked = true;
            document.getElementById("local").value = "https://arcadiso365.sharepoint.com/sites/CorporativoBR/acervotecnico/Sistema%20Acervo/Forms/Base2.aspx";
            
          }
        }
      }
    }
    document.getElementById("carregando").hidden = true;
    document.getElementById("mensagem").innerHTML = "";
  }

////DRO///////
/*xmlhttpDRO.onload = function () {
  if (xmlhttpDRO.readyState === xmlhttpDRO.DONE) {
    if (xmlhttpDRO.status === 200) {
      var s = JSON.parse(xmlhttpDRO.responseText);
      var keys = [];
      for(var k in s) keys.push(k);
        var ordem = [];
        ordem[0] = document.getElementById("jan").innerHTML;
        ordem[1] = document.getElementById("fev").innerHTML;
        ordem[2] = document.getElementById("mar").innerHTML;
        ordem[3] = document.getElementById("abr").innerHTML;
        ordem[4] = document.getElementById("mai").innerHTML;
        ordem[5] = document.getElementById("jun").innerHTML;
        ordem[6] = document.getElementById("jul").innerHTML;
        ordem[7] = document.getElementById("ago").innerHTML;
        ordem[8] = document.getElementById("set").innerHTML;
        ordem[9] = document.getElementById("out").innerHTML;
        ordem[10] = document.getElementById("nov").innerHTML;
        ordem[11] = document.getElementById("dec").innerHTML;
      }
  }
}*/
        xmlhttpOneLiner.send(JSON.stringify(json)); 
        xmlhttpOneLiner.responseType = 'text';
        xmlhttpProjetoAno.send(JSON.stringify(json)); 
        xmlhttpProjetoAno.responseType = 'text';
        xmlhttpLPR.send(JSON.stringify(json)); 
        xmlhttpLPR.responseType = 'text';
        xmlhttpConsorcio.send(JSON.stringify(json)); 
        xmlhttpConsorcio.responseType = 'text';
        xmlhttpArt.send(JSON.stringify(json)); 
        xmlhttpArt.responseType = 'text';
        xmlhttpRoot1.send(JSON.stringify(json)); 
        xmlhttpRoot1.responseType = 'text';
        //xmlhttpDRO.send(JSON.stringify(json)); 
        //xmlhttpDRO.responseType = 'text';
})

////formatacao de tabela//////
function variacao(target,encerramento,variacao){
    document.getElementById(variacao).value=document.getElementById(encerramento).value-document.getElementById(target).value;
    document.getElementById(variacao).innerHTML = parseFloat(document.getElementById(variacao).value).toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
}

function mediadro(){
  if((!(isNaN(document.getElementById("mes1").value)))&& (!(isNaN(document.getElementById("mes2").value)))&& (!(isNaN(document.getElementById("mes3").value)))&& (!(isNaN(document.getElementById("mes4").value)))&& (!(isNaN(document.getElementById("mes5").value)))&& (!(isNaN(document.getElementById("mes6").value)))&& (!(isNaN(document.getElementById("mes7").value)))&& (!(isNaN(document.getElementById("mes8").value)))&& (!(isNaN(document.getElementById("mes9").value)))&& (!(isNaN(document.getElementById("mes10").value)))&& (!(isNaN(document.getElementById("mes11").value)))&& (!(isNaN(document.getElementById("mes12").value)))){
debugger
    document.getElementById("media").value = (parseFloat(document.getElementById("mes1").value) + parseFloat(document.getElementById("mes2").value) + parseFloat(document.getElementById("mes3").value) +parseFloat(document.getElementById("mes4").value) +parseFloat(document.getElementById("mes5").value) +parseFloat(document.getElementById("mes6").value) +parseFloat(document.getElementById("mes7").value) +parseFloat(document.getElementById("mes8").value) +parseFloat(document.getElementById("mes9").value) +parseFloat(document.getElementById("mes10").value) +parseFloat(document.getElementById("mes11").value) +parseFloat(document.getElementById("mes12").value))/12;
    document.getElementById("media").innerHTML=document.getElementById("media").value.toLocaleString('pt-BR', { minimumFractionDigits: 2,maximumFractionDigits: 2});
    debugger
  }
  else{
    document.getElementById("media").value = "Apenas números";
    document.getElementById("media").innerHTML = document.getElementById("media").value;
}
}