import { ShowAlert } from "../../../libs/ShowAlert.js";

class Pesquisa {
  constructor() {
    this.token = this.setToken();
  }

  getToken() {
    return this.token;
  }
  setToken() {
    const srchField = window.location.search;
    const params = new URLSearchParams(srchField);
    return params.get("tkn");
  }

  async sendSurvey(possibleMessages) {
    const answers = this.getAnswers();

    if(!answers){
      ShowAlert.error(possibleMessages.blankfieldsMessage);
      return
    }

    ShowAlert.loading(possibleMessages.loadingMessage, possibleMessages.loadingTitle)
    return await axios
    .post("action/survey.php", {answers: answers, token: this.token}, {
      before: () => {
      }
      })
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        return error;
      });
  }

  validateInput(inputValue) {
    if (inputValue == "") {
      return null;
    } else {
      return inputValue;
    }
  }

  getSelectedOption(elementName) {
    let response = "";
    document.getElementsByName(elementName).forEach((option) => {
      option.checked ? (response = option.id) : null;
    });
    return response;
  }

  getAnswers() {
    const answers = [
      {question: 1, answer: this.validateInput($("#txt-q1").val())},
      {question:2,  answer: this.validateInput($("#txt-q2").val())},
      {question:3,  answer: this.validateInput(this.getSelectedOption("check-authorize"))},
      {question:4,  answer: this.validateInput($("#select-q4").val())},
      {question:5,  answer: this.validateInput($("#select-q5").val())},
      {question:6,  answer: this.validateInput($("#select-q6").val())},
      {question:7,  answer: this.validateInput($("#select-q7").val())},
      {question:8,  answer: this.validateInput($("#select-q8").val())},
      {question:9,  answer: this.validateInput($("#select-q9").val())},
      {question:10,  answer: this.validateInput($("#select-q10").val())},
      {question:11,  answer: this.validateInput($("#select-q11").val())},
      {question:12,  answer: this.validateInput(this.getSelectedOption("check-recommend"))},
      {question:13,  answer: $("#txt-q13").val() == "" ? "no-answer" : $("#txt-q13").val()},
      {question:14,  answer: $("#txt-q14").val() == "" ? "no-answer" : $("#txt-q14").val()},
    ];
    
    let check = true;
    answers.forEach(objAnswer => {
      if(objAnswer.answer == null) {
        check = false
      }
    })
    
    return !check ? check : answers;
  }
}

export { Pesquisa };
