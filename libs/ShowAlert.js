class ShowAlert{
  static success(textSuccessAlert, titleSuccess){
    Swal.fire({
      position: "center",
      icon: 'success',
      title: titleSuccess == "" ? 'Tudo certo!' : titleSuccess,
      text: textSuccessAlert,
    })
  }
  static error(textErrorAlert, titleError){
    Swal.fire({
      position: "center",
      icon: 'error',
      title: titleError == "" ? 'Algo deu errado!' : titleError,
      text: textErrorAlert,
    })
  }
  static loading(textLoadingAlert, titleLoading){
    Swal.fire({
      position: "center",
      title: titleLoading == "" ? "Por favor, aguarde.." : titleLoading,
      text: textLoadingAlert,
      showConfirmButton: false,
      timer: 15000,
      // allowEscapeKey: false,
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
  }
  static afterLoadSuccess(){
    Swal.fire({
      position: "center",
      icon: "success",
      text: "A ferramenta está pronta para o uso",
      showConfirmButton: false,
      timer: 1000,
    });
  }
  static afterLoadError(){
    Swal.fire({
      position: "center",
      icon: "error",
      text: "Algo deu errado, atualize a página.",
      showConfirmButton: false,
      timer: 1000,
    });
  }
  static actionSuccess(details){
    Swal.fire({
      icon: 'success',
      title: 'Tudo certo',
      html: `Ação realizada com sucesso!<br>
             ${details}`,
    })
  }
}

export { ShowAlert };