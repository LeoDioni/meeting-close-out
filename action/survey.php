<?php
require 'powerPlatform/FlowRequest.php';
$json_str = file_get_contents('php://input');
// var_dump($json_str);
// return;
$objSurvey = json_decode($json_str, true);

if (validateInvalidAnswers($objSurvey['answers'])) {
  try {
    $flowEndpoint = "https://prod-17.westus.logic.azure.com:443/workflows/d261aaeb19384f22884a25b9ccd3b9c0/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=rA2zrCYgXh7Ffok9t83W7uaBO5cUq2qLm8C5J3YNS0I";
    $request = FlowRequest::curlPostRequest($objSurvey, $flowEndpoint);
    echo json_encode($request);
  } catch (\Throwable $th) {
    //throw $th;
  }
} else {
}

function validateInvalidAnswers($answers)
{
  $allValid = true;
  foreach ($answers as $answer) {
    if ($answer == null) {
      $allValid = false;
      return $allValid;
    }
  };
}

function sendSurvey()
{
}
